
import './App.css';
import Product from './pages/Product';
import ProductList from './components/ProductList';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Cart from './pages/Cart';
import Payment from './pages/Pay';
import Sucess from './pages/Sucess';
import {BrowserRouter, Route, Routes,Navigate}  from "react-router-dom"
import {useSelector} from "react-redux";


function App() {
    //DEFINE THE USESELECTOR HOOK TO GRT THE CURRENTUSER STAETE IN REDUX
    const user =  useSelector(state=> state.user. currentUser)
  
  return (
    <BrowserRouter>
    
    <Routes>
       <Route exact path="/" element={<Home/>} />
       <Route path="/products/:category" element={<ProductList/>} />
       <Route path="/product/:productId" element={<Product/>} />
       <Route path="/cart" element={<Cart/>} />


       <Route path="/login" element={user ? <Navigate to ="/" /> :<Login/>}  />
  
       <Route path="/register" element={ user ? <Navigate to ="/" /> :<Register/>} />
        {/* <Route path="/pay" element={<Payment/>} /> */}
        <Route path="/sucess" element={<Sucess/>} />
  
    </Routes>
     
    
  
    </BrowserRouter>
    

  );
}

export default App;
