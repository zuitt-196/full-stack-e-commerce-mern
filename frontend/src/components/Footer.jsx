
import React from 'react'
import styled from 'styled-components'
import {Facebook, Instagram ,Twitter, Pinterest, Mail, LocalPhone, MyLocation} from '@material-ui/icons'
import { paymentImage } from '../data'


import {sliderItems } from "../data"


// IMPORT RESPONSIVEs
import {mobile} from '../responsive';

const Footer = () => {
  return (
    <Container>
        
        <Left>
                <Logo>
                    VHONG
                </Logo>

                <Description>
                    There have many various clothes for those people, who are exposed to fashion in their life, let you find my shop and chose what your like, There have a lot of sizes and styles that I recommend to 
                    </Description>
             <SocialContainer>
                        <SocialIcon color="0000FF">
                            <Facebook/>
                        </SocialIcon>

                        <SocialIcon color='f90606'>
                                <Instagram />
                        </SocialIcon>

                        <SocialIcon color='55ACEE'>
                                <Twitter/>
                        </SocialIcon>
                        
                            
                        <SocialIcon color='E60023'>
                                <Pinterest/>
                        </SocialIcon>
                        
                        

             </SocialContainer>

      
        </Left>

              <Center>
                  
                <Title>
                    Useful Links
                </Title>
                <List>
                    <ListItem>Home</ListItem>
                    <ListItem>Cart</ListItem>
                    <ListItem>Man Fashion</ListItem>
                    <ListItem>Woman Fashion</ListItem>
                    <ListItem>Accesories</ListItem>
                    <ListItem>My Account</ListItem>
                    <ListItem>Order Trackin</ListItem>
                    <ListItem>Wislist</ListItem>
                    <ListItem>Terms</ListItem>
                </List>
             </Center>


        <Right> 
            <Title>
                    Contact
            </Title>
                    <Contactitem>
                        <MyLocation style={{marginRight:"10px"}}/> Cumawas, Bislig city ,Surigao Del Sur , Philiphines
                    </Contactitem>

                    <Contactitem>
                        <LocalPhone style={{marginRight:"10px"}}/>+ 09-10-27-64-396P s
                    </Contactitem>

                    <Contactitem>
                        <Mail style={{marginRight:"10px"}}/>BercasioVhog@gmail.com
                    </Contactitem>

                    <Payment src="https://www.tracycosmetic.com/wp-content/uploads/2021/11/PayPal-payment-methods.jpeg"/>
        </Right>
    </Container>
      

  )
}

export default Footer


const Container = styled.div`
    display:flex;
    ${mobile({flexDirection :"column", padding: "2px"})};
    

`

const Logo = styled.h1`

`

const Left = styled.div`
    flex:1; 
    display:flex;
    flex-direction:column;
    padding: 20px;

`

const Description = styled.p`
    margin: 20px 0;
`


const SocialContainer = styled.div`
    display:flex;


`

const SocialIcon = styled.div`
        width:40px;
        height: 40px;
        border-radius: 50%;
        background-color: #${props => props.color};
        display:flex;
        align-items:center;
        justify-content: center;
        margin-right: 15px;
`



const  Center = styled.div`
flex:1;
padding: 20px;
// background:  red;
${mobile({ display:"none"})};

`

const Title = styled.h3`
margin-bottom: 30px;
`
const List = styled.ul`
    margin:0;
    padding:0;
    list-style:none;
    display:flex;
    flex-wrap: wrap;
`

const ListItem = styled.li`
    width:50%;
    margin-bottom: 10px;
`





const Right = styled.div`
flex:1;
padding: 20px;
${mobile({backgroundColor:"#eeee"})};
`


const Contactitem = styled.div`
    display:flex;
    align-items: center;
    margin-bottom:20px;

`

const Payment = styled.img`
    width:40%;
    margin-bottom:20px;
`   
