

import React from 'react'
import styled from 'styled-components'
import {Link} from "react-router-dom"

// IMPORT RESPONSIVE
import {mobile} from '../responsive'


const CategoryItem = ({item}) => {
  // console.log("item:",item.title);
  return (
    <Container>
        <Link to  = {`/products/${item.category}`}>
          <Image src={item.img}/>
          <Info>  
            <Title>{item.title}</Title>
              <Button>SHOP NOW</Button>
          </Info>
          </Link>
    </Container>
  )
}

export default CategoryItem

const Container = styled.div`
  flex:1;
  margin:7px;
  height:60vh;
  display:flex;
  align-item:center;
  justify-content:center;
  position:relative;
  ${mobile({height :"30px"})};




`


const Image = styled.img`
width:400px;
height:400px;
object-fit: cover; 
z-index:1;
${mobile({height :"40vh"})};





`


const Info = styled.div`
 position:absolute;
 width:100%;;
 height:400px;
 top:0;
 left:0;
 display:flex;
 flex-direction:column;
 align-items:center;
 justify-content:center;
//  background-color: red;
`

const Title = styled.h1`
 font-weight: 800;
 font-size: 25px;
 margin-bottom:20px;
 color: white;

 
`

const Button = styled.button`
 padding: 10px;
 border:none;
 backgroun-color: white;
 color:gray;
 font-weight: 800;

 &:hover{
    color: green;
 }
`