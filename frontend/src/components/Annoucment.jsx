import React from 'react'
import styled from 'styled-components';

// IMPORT RESPONSIVE
import {mobile} from '../responsive'

const Annoucment = () => {
  return (
        <Container>
            Tara sa akong Tindahan Online Shop!! Super Deal Free Shipping on Order $50
        </Container>
  )
}

export default Annoucment


const Container = styled.div`
    height:30px;
    background-color: teal;
    color:white;
    display:flex;
    align-items: center;
    justify-content: center;
    font-size:14px;
    font-weight: 800;
    ${mobile({textAlign: 'center'})};

    

`