import React,{useState,useEffect} from 'react'
import styled from 'styled-components'

import Product from './Product';
import { popularProducts} from "../data"
import axios from 'axios';




const Products = ({category,filters,sort}) => {
  
      // DEFINE  STATE OF PRODUCTS
      const [products, setProducts] = useState([])

      //DEFINE STATE OF FILTES
      const [filterProducts, setFilterProducts] = useState([])


      //DEFIEN THE SIDE EFFECT AFTER REDERING THE COMPONENTS THE GET THE API PRODUCTS WITH THE CATERGORY
      useEffect(() => {
            const getProducts = async () =>{
                try {
                    const respones = await axios.get( category ? `http://localhost:5000/api/product?category=${category}`:"http://localhost:5000/api/product" );
                    // console.log(respones.data)
                    setProducts(respones.data)
                } catch (error) {
                    console.log(error)
                }
            }
            getProducts();
      }, [setProducts])

      
      // DEFINE THE THE FILTER PRODUCT BY GETTUNG THE DATABASE
      useEffect(() => {
        category && setFilterProducts(products.filter(item => Object.entries(filters).every(([key,value]) =>
            item[key].includes(value)    

        )))
      }, [category, filters, products]);



 
      // DEFINE THE SORT PRODUCTS

      useEffect(() => {
        if (sort === "newest") {
          setFilterProducts((prevestate) =>  [...prevestate].sort((a,b) => a.createdAt - b.createdAt))      
        }else if(sort === "asc"){
          setFilterProducts((prevestate) =>  [...prevestate].sort((a,b) => a.price - b.price))
      
        }else{
          setFilterProducts((prevestate) =>  [...prevestate].sort((a,b) => b.price - a.price)) 
        }
      }, [sort])

  return (
    <Container>
      {category ?filterProducts.map((item, index) => ( 
            <Product key={index} item={item}/>)) :
            products.slice(0,4).map((item, index) => <Product item={item} key={index}/>)}
    </Container> 
  )
}

export default Products


const Container = styled.div`
        padding: 20px;
        display: flex;
        flex-wrap:wrap;
        justify-content:space-between;

    `