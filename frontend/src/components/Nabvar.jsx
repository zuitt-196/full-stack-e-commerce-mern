import React from 'react'
import styled from 'styled-components';
import {Search, ShoppingCart} from '@material-ui/icons'
import { Badge } from '@material-ui/core'
import {Link } from "react-router-dom"
// IMPORT RESPONSIVE
import {mobile} from '../responsive'

import {useSelector} from "react-redux";


const Nabvar = () => {
        // DEFINE THE STATE FROMV THE REDUCS STATE 
    const quantity = useSelector(state =>state.cart.quantity)
        
    console.log(quantity);
  return (
            <Container>
                    <Wrapper>
                            {/* LEFT LAYOUT SECTION START */}
                            <Left>
                                    <Language>EN</Language>

                                            <SearchContainer>
                                                 <Input placeholder='search'/>
                                                <Search style={{color:"gray", fontSize:"16px"}}/>
                                            </SearchContainer>
                            </Left>
                            {/* LEFT LAYOUT SECTION END */}         

                                {/* CENTER LAYOUT SECTION START */}                
                                    <Center><Logo >VHONG.</Logo></Center>

                                 {/* CENTER LAYOUT SECTION END */}

                               {/* RIGHT LAYOUT SECTION START */}        
                                   <Right> 

                                     <Link to ="/register"><Menuitem>REGISTER</Menuitem></Link> 
                                       <Link to ="/login">    <Menuitem>SIGN IN</Menuitem></Link>


                                <Link to ="/cart">
                                            <Menuitem>
                                                    <Badge badgeContent={quantity} color="primary" overlap="rectangular">
                                                            <ShoppingCart/>
                                                    </Badge>
                                                                                
                                            </Menuitem>

                                    </Link>

                                   </Right>
                                 {/* CENTER LAYOUT SECTION END */}     
                    </Wrapper>

            </Container>
    
  )
}

export default Nabvar


// stylecomponets 
const Container = styled.div`
        height: 60px;
    // SMALL DEVICE VIEWPORT
    ${mobile({height:"50px"})};




`

const Wrapper = styled.div`
    padding :10px 20px;
    display:flex;
    justify-content:space-between;
    align-items: center;
    ${mobile({padding:"10px 0px"})};

    


`
    

// LEFT STYLED SECTION  

const Left = styled.div`
    flex:1;
    display:flex;
    align-items: center;
 
        

`


const Language = styled.span`
    font-size: 14px;
    cursor:pointer;
    ${mobile({display:"none"})};



`
const SearchContainer = styled.div`
        // background:pink;
        border: 0.5px solid lightgray;
        display: flex;
        align-items: center;
        padding: 5px;
        margin-left: 25px;
       
    `    
// if you notice it used input element to define
 const Input = styled.input`
 border: none;
 outline:none;
 ${mobile({width:"50px"})};

  `   

// END LEFT  STYLED SECTION




// START STYLED CENTER SECTION 
const Center = styled.div`
    flex:1
`

const Logo = styled.h1`
    font-weight: bold;
    text-align: center;
    ${mobile({fontSize:"24px"})};

`

// END STYLED CENTER SECTION 




// START STYLED RIGHT SECTION 
const Right = styled.div`
    flex:1;
    display:flex;
    align-items:center;
    justify-content:flex-end;
    ${mobile({ flex: 2, justifyContent: "center"})};

`

const Menuitem = styled.div`
    font-size: 14px;
    cursor: pointer;
    margin-left: 25px;
    ${mobile({fontSize:"12px", marginLeft: "10px"})};

`





