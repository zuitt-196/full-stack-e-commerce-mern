import React from 'react'
import './NewUser.css';

const NewUser = () => {
  return (
    <div className="new-user-container">
            <h1 className="new-user-title">New User</h1>
            <form className="new-user-form">
                    <div className="new-user-item">
                            <label>Username</label>
                            <input type="text" placeholder="bong" />
                    </div>
                    <div className="new-user-item">
                            <label>Full Name</label>
                            <input type="text" placeholder="Vhong Bercasio" />
                    </div>

                    <div className="new-user-item">
                            <label>Email</label>
                            <input type="email" placeholder="Bercasio@email.com"/>
                    </div>
                    
                    <div className="new-user-item">
                            <label>Password</label>
                            <input type="password" placeholder="sdsd" />
                    </div>
                    <div className="new-user-item">
                           <label>Phone</label>
                            <input type="text" placeholder="0902342358" />
                    </div>
                    <div className="new-user-item">
                            <label>Address</label>
                            <input type="text" placeholder="davao" />
                    </div>

                    <div className="new-user-item">
                            <label>Gender</label>
                            <div className="new-user-gender">
                                    <input type="radio" name="gender" id="male"  value="male"/>
                                    <label for="male">Male</label>
                                    
                                    <input type="radio" name="gender" id="female"  value="female"/>
                                    <label for="male">Female</label>
                                    
                                    <input type="radio" name="gender" id="others"  value="others"/>
                                    <label for="others">Others</label>
                            </div>
                    </div>

                    <div className="new-user-item">
                            <label>Active</label>
                            <select className='new-user-select' name='active' id="active">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                             </select>
                    </div>

                  
                    <button className="new-user-button">
                            Create
                    </button>
            </form>
          
        </div>
  )
}

export default NewUser