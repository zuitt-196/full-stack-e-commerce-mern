import React from 'react'
import './WidgetSmall.css'
import VisibilityIcon from '@mui/icons-material/Visibility';

const WidgetSmall = () => {
  return (
    <div className="widget-small-contiainer">
      <span className='widget-small-title'>
            New Join Members
      </span>
      <ul className="widget-small-list">
            <li className="widegt-small-list-item">
                    <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt="image" className="widget-small-image" />
                    <div className="widget-small-user">
                            <span className="widget-small-username">
                                    vhong Bercasio
                            </span>
                            <span className="widget-small-username-job-title">
                                    Software  Engineer
                            </span>
                    </div>
                    <button className="widget-small-button">
                    <VisibilityIcon  className="widget-small-icon"/>
                    Display
                    </button>
            </li>

            <li className="widegt-small-list-item">
                    <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt="image" className="widget-small-image" />
                    <div className="widget-small-user" >
                            <span className="widget-small-username">
                                    vhong Bercasio
                            </span>
                            <span className="widget-small-username-job-title">
                                    Software  Engineer
                            </span>
                    </div>
                    <button className="widget-small-button">
                    <VisibilityIcon  className="widget-small-icon"/>
                    Display
                    </button>
            </li>

            <li className="widegt-small-list-item">
                    <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt="image" className="widget-small-image"/>
                    <div className="widget-small-user">
                            <span className="widget-small-username">
                                    vhong Bercasio
                            </span>
                            <span className="widget-small-username-job-title">
                                    Software  Engineer
                            </span>
                    </div>
                    <button className="widget-small-button">
                    <VisibilityIcon  className="widget-small-icon"/>
                    Display
                    </button>
            </li>

            <li className="widegt-small-list-item">
                    <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt="image" className="widget-small-image"/>
                    <div className="widget-small-user">
                            <span className="widget-small-username">
                                    vhong Bercasio
                            </span>
                            <span className="widget-small-username-job-title">
                                    Software  Engineer
                            </span>
                    </div>
                    <button className="widget-small-button">
                    <VisibilityIcon  className="widget-small-icon"/>
                    Display
                    </button>
            </li>

            <li className="widegt-small-list-item">
                    <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt="image" className="widget-small-image"/>
                    <div className="widget-small-user">
                            <span className="widget-small-username">
                                    vhong Bercasio
                            </span>
                            <span className="widget-small-username-job-title">
                                    Software  Engineer
                            </span>
                    </div>
                    <button className="widget-small-button">
                    <VisibilityIcon className="widget-small-icon"/>
                    Display
                    </button>
            </li>

            <li className="widegt-small-list-item">
                    <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt="image" className="widget-small-image"/>
                    <div className="widget-small-user">
                            <span className="widget-small-username">
                                    vhong Bercasio
                            </span>
                            <span className="widget-small-username-job-title">
                                    Software  Engineer
                            </span>
                    </div>
                    <button className="widget-small-button">
                    <VisibilityIcon className='widget-small-icon'/>
                    Display
                    </button>
            </li>
      </ul>
    </div>
  )
}

export default WidgetSmall























