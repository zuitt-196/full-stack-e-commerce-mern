import React from 'react'
import './WidgetLarge.css'

const WidgetLarge = () => {
    // DEFINE A SMALL COMPONENT

    const Button = ({type}) =>{
            return <button className={"widget-large-button " + type} >{type}</button>
    }

  return (
    <div className="widget-large-conatainer">
        <h3 className="widget-large-title">
                Latest Transcation
        </h3>
        <table className="widget-large-table">
                <tbody>

                <tr className="wigdet-large-tr">
                        <th className="widget-large-th">
                                Customer
                        </th>
                        <th className="widget-large-th">
                                Date
                        </th>

                        <th className="widget-large-th">
                                Amount
                        </th>
                        <th className="widget-large-th">
                                Status
                        </th>
                </tr>
                    <tr className='widget-large-tr'>
                            <td className="widget-large-user">
                                 <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt=""  className="widget-large-img"/>
                                    <span className="widget-large-name">
                                            Rio boy
                                    </span>
                            </td>
                            <td className="widget-large-date">
                                    2 jun 2023
                            </td>
                            <td className="widget-large-amount">
                                    $122.000
                            </td>

                            <td className="widget-large-status">
                            <Button type="Declined"/>
                            </td>
                    </tr>

                    <tr className='widget-large-tr'>
                            <td className="widget-large-user">
                                 <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt=""  className="widget-large-img"/>
                                    <span className="widget-large-name">
                                            Rio boy
                                    </span>
                            </td>
                            <td className="widget-large-date">
                                    2 jun 2023
                            </td>
                            <td className="widget-large-amount">
                                    $122.000
                            </td>

                            <td className="widget-large-status">
                                <Button type="Pending"/>
                            </td>
                    </tr>

                    <tr className='widget-large-tr'>
                            <td className="widget-large-user">
                                 <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt=""  className="widget-large-img"/>
                                    <span className="widget-large-name">
                                            Rio boy
                                    </span>
                            </td>
                            <td className="widget-large-date">
                                    2 jun 2023
                            </td>
                            <td className="widget-large-amount">
                                    $122.000
                            </td>

                            <td className="widget-large-status">
                                <Button type="Approved"/>
                            </td>
                    </tr>

                    <tr className='widget-large-tr'>
                            <td className="widget-large-user">
                                 <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt=""  className="widget-large-img"/>
                                    <span className="widget-large-name">
                                            Rio boy
                                    </span>
                            </td>
                            <td className="widget-large-date">
                                    2 jun 2023
                            </td>
                            <td className="widget-large-amount">
                                    $122.000
                            </td>

                            <td className="widget-large-status">
                                <Button type="Approved"/>
                            </td>
                    </tr>

                    <tr className='widget-large-tr'>
                            <td className="widget-large-user">
                                 <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt=""  className="widget-large-img"/>
                                    <span className="widget-large-name">
                                            Rio boy
                                    </span>
                            </td>
                            <td className="widget-large-date">
                                    2 jun 2023
                            </td>
                            <td className="widget-large-amount">
                                    $122.000
                            </td>

                            <td className="widget-large-status">
                                <Button type="Approved"/>
                            </td>
                    </tr>

                    <tr className='widget-large-tr'>
                            <td className="widget-large-user">
                                 <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfB6Bfg2nmnnuer98-ZS28mH7MqddkvG63MJQtr5Xomtag&oe=63E93111" alt=""  className="widget-large-img"/>
                                    <span className="widget-large-name">
                                            Rio boy
                                    </span>
                            </td>
                            <td className="widget-large-date">
                                    2 jun 2023
                            </td>
                            <td className="widget-large-amount">
                                    $122.000
                            </td>

                            <td className="widget-large-status">
                                <Button type="Approved"/>
                            </td>
                    </tr>
                        
                    </tbody>
        </table>
    </div>
  )
}

export default WidgetLarge
