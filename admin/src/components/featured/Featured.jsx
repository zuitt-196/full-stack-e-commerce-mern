import React from 'react'
import  './Featured.css'
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';

const Featured = () => {
  return (
    <div className="featured-container">
            <div className="featured-item">
                    <span className="fetured-title">Revenue</span>
                    <div className="fetured-money-container">
                            <span className="fetured-money">$2,415</span>
                            <span className="fetured-money-rate">-11.4 <ArrowDownwardIcon className="fetured-icon negative"/></span>
                        </div>
                        <span className="featured-sub">
                            Compared to last month
                        </span>
            </div>

            <div className="featured-item">
                    <span className="fetured-title">Revenue</span>
                    <div className="fetured-money-container">
                            <span className="fetured-money">$2,415</span>
                            <span className="fetured-money-rate">-11.4 <ArrowDownwardIcon className= "fetured-icon negative"/></span>
                        </div>
                        <span className="featured-sub">
                            Compared to last month
                        </span>
            </div>

            <div className="featured-item">
                    <span className="fetured-title">Cost</span>
                    <div className="fetured-money-container">
                            <span className="fetured-money">$4.415</span>
                            <span className="fetured-money-rate">+2.4 <ArrowUpwardIcon className="fetured-icon"/></span>
                        </div>
                        <span className="featured-sub">
                            Compared to last month
                        </span>
            </div>
    </div>
  )
}

export default Featured