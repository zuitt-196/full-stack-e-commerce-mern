import {createSlice} from "@reduxjs/toolkit";


const initialState = {
    currentUser:null,
    isFetching:false,
    error:false,
    urlLogin:null
}


const userSlice = createSlice({   
    name: 'user',
    initialState,
    reducers:{
            LOGIN_START:(state) =>{
                // this  isFetching conviction maybe used is isLoading as emphasize 
                 state.isFetching= true;
            },
            LOGIN_SUCCES:(state,action) =>{
                    state.isFetching= false;
                    state.currentUser = action.payload;
                    // console.log("state.currentUser:",state.currentUser ? true: false);

            },
            URL_LOGIN:(state,action) =>{   
                state.urlLogin =action.payload;
            },

            LOGIN_ERROR:(state) =>{   
                state.isFetching= false;
                state.error = true;
            }

    }


    }

);




export const {LOGIN_START, LOGIN_SUCCES,LOGIN_ERROR, URL_LOGI} = userSlice.actions;

export default userSlice.reducer;