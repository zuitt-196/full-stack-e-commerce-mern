const mongoose = require('mongoose');

const ProductSchema =  new  mongoose.Schema({

        title:{
            type: String,
            required: [true, "plase add Product"],
            unique:true,
        },

        desc:{
            type: String,
            required: [true, "please add s Discription"],
            

        },

        image:{
                type: String,
                required: [true, "please add Image"],

        },
        categories:{
            type: Array,
            required: [true, "please add Categories"],
          
       },
        size:{
        type: Array,
        required: [true, "please add Size"],

        },

        color:{
        type: Array,
            required: [true, "please add Image"],

       },
       price:{
        type: Number, 
        required: [true, " Please add Price "],

       },
       inStaock:{
            type: Boolean,
            default: true,
       }


},
    {
        timestamps:true
    }
)

module.exports = mongoose.model('Product', ProductSchema);