const mongoose = require('mongoose');

const OrderSchema =  new  mongoose.Schema({

        userId:{
            type: String,
            required: true
        },

        // So this Document has arry type which contain of what kind of project their hava get
        products:[
            {
                productId:{
                        type: String,

                },
                quantity:{
                    type: Number,
                    default:1
                }
            }
        ],


        amount: {
                type: Number,
                required: true
        },
        address:{
                type:Object,
                required: true

        },
        status: {
            type: String,
            default: "Pending"
            
        }
          
        

},
    {
        timestamps:true
    }
)

module.exports = mongoose.model('Order ', OrderSchema);