const express = require('express');
const cors = require('cors');
const app = express();
const mongoose= require('mongoose');
const dotenv = require('dotenv').config();
const bodyParser = require('body-parser');
const  userRoutes =  require('./routes/userRoutes');
const authRoutes = require('./routes/authRoutes');
const productRoutes = require('./routes/productRoutes');
const  orderRoutes = require('./routes/orderRoutes');
const cartRoutes = require('./routes/cartRoutes');
const stripeRoutes = require('./routes/stripeRoutes');

mongoose.set('strictQuery', false);


// MIDDLE SECTION BUILT IN  EXPRESS 
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:false}));
app.use(bodyParser.json())



mongoose.connect(process.env.MONGO_URL).then(() => {
    console.log("MongoDB connect success ");
}).catch(err => {
        console.log(err);
});


// REST API SECTION 
app.use('/api/auth', authRoutes);
app.use("/api/users", userRoutes);
app.use("/api/product", productRoutes);
app.use("/api/orders", orderRoutes);
app.use("/api/carts", cartRoutes);
app.use("/api/checkout", stripeRoutes);






// PORT SECTION
app.listen(process.env.PORT || 5000, () =>{
        console.log("Backend Server is running ");
})


