const { verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin } = require('./verifyToken');
const Cart = require('../models/CartModel');
const router =  require('express').Router();

// POST METHOD HTTP ADD THE CART 
router.post("/", verifyToken,async(req, res) => {
    const newCart = new Cart(req.body);

    try {
        const savedCart = await newCart.save();
        res.status(200).json(savedCart);
    } catch (error) {
        res.status(500).json({message: error.message});
    }

})
  

//UPDATE  HTTP METHOD UPDATE THE CAET
router.put("/id", verifyTokenAndAuthorization,async(req, res) => {
    try {
        const updatedCart = await Cart.findByIdAndUpdate(
                req.params.id,

            {
                $set: req.body
            },
              {  
                new:true
              }
            )

          res.staut.json(updatedCart)
    } catch (error) {
        res.status(500).json({message: error.message});
    }
})


// DELETE CART HTTPP METHOD

router.delete("/:id" , verifyTokenAndAuthorization , async (req, res) => {
        try {
            await Cart.findByIdAndDelete(req.params.id);
            res.status(200).json("Cart has been deleted")
        } catch (error) {
            res.status(500).json({message: error.message});
        }
})


//  GET HTTP METHOD GET USER  CART THE ID OF USER 
router.get("/find/:userId", verifyTokenAndAuthorization, async(req, res) => {
        try {
            const cart= await Cart.findBy({userId: req.params.userId});
                res.status(200).json(cart);
        } catch (error) {
            res.status(500).json({message: error.message});
        }
})

// GET ALL CART PRODUCT

router.get('/', verifyTokenAndAdmin ,async (req, res) => {
        try {
            const carts = await Cart.find()
            res.status(200).json(carts)
        } catch (error) {
            res.status(500).json({message: error.message});
        }
})


module.exports  = router