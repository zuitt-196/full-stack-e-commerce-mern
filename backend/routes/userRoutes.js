const  router  = require('express').Router();
const User = require('../models/UserModel')
const {verifyTokenAndAuthorization ,verifyTokenAndAdmin }= require("./verifyToken");




// PUT HTTP METHOD UPDATE THE USER
router.put("/:id", verifyTokenAndAuthorization, async (req, res) => {
    if (req.body.password) {
        req.body.password = CryptoJS.AES.encrypt(
        req.body.password,
        process.env.JWT_SECRET
      ).toString();
    }
  
    try {
      const updatedUser = await User.findByIdAndUpdate(
        req.params.id,
        {
          $set: req.body,
        },
        { new: true }
      );
      res.status(200).json(updatedUser);
    } catch (err) {
    return res.status(500).json(err);
    }
  });


  // DELETE HTTOP METHOD 

  router.delete("/:id", verifyTokenAndAuthorization, async (req, res) => {
        try {
            await User.findByIdAndDelete(req.params.id)
            res.status(200).json("User has been deleted....")
        } catch (err) { 
            return res.status(500).json(err);
        }

  })


    // ADMIN AREA

/// GET HTTP METHOD  TO GET THE USER
  router.get("/find/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
      const user = await User.findById(req.params.id);
      const {password, ...others} = user._doc;
        res.status(200).json(others);
    } catch (err) { 
        return res.status(500).json(err);
    }

})

// GET ALL USER
router.get("/", verifyTokenAndAdmin, async (req, res) => {
    const query = req.query.new



    try {
      const user =  query ? await User.find().sort({_id: -1}).limit(1): await User.find();
            // console.log(" query ? await User.find().sort({_id: -1}):",  query ? await User.find().sort({_id: -1}): null);


        res.status(200).json(user);
    } catch (err) { 
        return res.status(500).json(err);               
    }

})


/// FURTEHER  DETAIL THAT MUST BE NOTICE IN TERMS OF GETTING THE LAST ARRAY IN AN OBJECT (- 1   )

// GET USER STATUS  
router.get("/stats", verifyTokenAndAdmin, async (req, res) => {
      const  date = new Date();
      console.log("date:", date);

      const lastYear = new Date(date.setFullYear(date.getFullYear() - 1));
     console.log("lastYear:", lastYear);

    // thes exprsession wad define the number of month that craeted 
      try {

            const data = await User.aggregate([
                { $match : {createdAt:{$gte:lastYear}}},
                {
                    $project:{
                         month: {$month: "$createdAt"},
                    }
                },       

                    {
                    $group: {
                      _id: "$month",
                      total: { $sum: 1 },
                    },
                  },
             

            ])

            console.log("data:", data)
            res.status(200).json(data)
        
      } catch (error) {
            res.status(404).json({ error: error})
      }
})



 
  module.exports = router;