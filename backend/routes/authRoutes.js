const  router  = require('express').Router();
;
// IMPORT SCHEMA FOR USERMODEL
const User = require("../models/UserModel");
// IMPORT CRYPTO JS
const CryptoJS= require("crypto-js");
const jwt = require("jsonwebtoken");

//REGITRATION SECTION ROUTES
router.post("/register", async (req, res) => {

    const newUser = await new User({
            username: req.body.username,
            email: req.body.email,
            password: CryptoJS.AES.encrypt(req.body.password, process.env.SECRET_KEY_PASS).toString() // ENCRYPT THT PASSWORD TO MUST SECURE
            
    });

    const {username, email, password} = newUser ;

    if (!username || !email || !password) {
        res.status(400).json("Must be Enter your credential")
    }


 
    // save into  
    try {
        const savedUser = await newUser.save(); 
        
            res.status(201).json(savedUser);
            
    } catch (error) {     
        res.status(500).json(error)


    }
})





// LOGIN SECTION ROUTES
router.post('/login', async (req, res) => {

try {                                                                            
    const user = await User.findOne({username: req.body.username})
    // console.log("user:", user.password)
    
    // valIdation OF USER IS NOT EXITS
    if (!user) {
        return  res.status(401).json({message:" User not found or wrong credential"})
    }


    
    // DECRYPT THE PASSWORD
    const hashedPassword =  CryptoJS.AES.decrypt(user.password, process.env.SECRET_KEY_PASS);   
    
    const passwordOriginal = hashedPassword.toString(CryptoJS.enc.Utf8);
    
    //VALIDATION  OF PASSWORD DID NOT MATCH
    if (passwordOriginal !== req.body.password) {
         return res.status(401).json({message: 'Invalid password or wrong credential'});
    }


                    
            // CREATE A TOKEM
            const accessToken = jwt.sign({
                    id:user._id,
                    isAdmin:user.isAdmin,
            }, process.env.JWT_SECRET, {expiresIn: "3d"})




        /// destructure the properties came from user
        const {password , ...others} = user._doc;


        res.status(200).json({...others , accessToken})
        // res.status(200).json({...others , accessToken})

       } catch (err) {
        res.status(400).json(err)
        
      
        
}
})



module.exports = router;



