const router = require('express').Router();
const Product = require('../models/ProductModel');
const {verifyTokenAndAuthorization ,verifyTokenAndAdmin }= require("./verifyToken");


//CREATE HTTP METHOD ADD PRODUCT
router.post("/", verifyTokenAndAdmin , async (req, res) => {
    const newProducty = await Product(
                req.body
        )
        try {
            const  saveProduct = await newProducty.save();
            res.status(200).json(saveProduct);
        } catch (error) {
            res.status(500).json({message: error.message})
        }
})


// UPDATE PRODUCT 
router.put("/:id", verifyTokenAndAdmin , async (req, res) => {
    try {
        const updateProduct = await Product.findByIdAndUpdate(req.params.id, 
            {
            $set:req.body
            },{
                new:true
            }
            )

    res.status(200).json(updateProduct)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})






// DELETE HTTP METHOD PRODUCT
router.delete('/delete/:id', verifyTokenAndAdmin , async (req, res) => {
    // res.send("Dlete product")
    try {
        await Product.findByIdAndDelete(req.params.id)   
        res.status(200).json("Product has been deleted")
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})


// GE PRODUCT BY ID
router.get('/find/:id', async (req, res) => {
        // res.send("finding single product")
        try {
            const product = await Product.findById(req.params.id);
            res.status(200).json(product);
          } catch (err) {
            res.status(500).json(err);
          }
})

// GET ALL PRODUCT 
router.get("/", async (req, res) => {
     
    const qNew = req.query.new;
    const qCategory = req.query.category;

    // further details we used $in method which is kind operator to define to match value and query URL
    try {
        let products; 
         if (qNew) {
            products= await Product.find().sort({ _id: - 1}).limit(1);  
         }else if(qCategory) {
            products= await Product.find({
                categories:{ 
                        $in:[ qCategory]
                }
                
            })
         }else{
                products = await Product.find()
         }
        res.status(200).json(products);
     
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})


module.exports  = router